/*
 El ministerio de seguridad de la provincia de Córdoba nos solicita una aplicación con ventanas que permita
cargar las multas de la caminera. Se necesita registrar dni del conductor, nombre del conductor, patente
del auto, descripcion de la multa y monto de la multa. Cuando se realiza el pago voluntario por parte del
infractor la multa tiene un descuento del 50%. Realizar una pantalla de carga de multas y que la misma
permita abrir otra ventana con los siguientes reportes:
• Cantidad de multas que tuvieron pago voluntario y que dicho monto pagado sea mayor a $1000.
• Suma de monto pagado general
• Promedio de monto pagado por pago voluntario
 */
package multas;
import javax.swing.JOptionPane;
/**
 *
 * @author Maka
 */
public class Principal extends javax.swing.JFrame {
    private Caminera caminera;
    /**
     * Creates new form Principal
     */
    public Principal() {
        initComponents();
        caminera = new Caminera();
        btnVerReportes.setEnabled(false);
    }
    
     public boolean validation(){
        if (txtNombre.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "El Nombre debe agregarse");
            txtNombre.requestFocus();
            return false;
        }
        if (txtDni.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "El DNI debe agregarse");
            txtDni.requestFocus();
            return false;
        }
        if (txtPatente.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "La Patente debe agregarse");
            txtPatente.requestFocus();
            return false;
        }
        if (txtDescripcion.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "La Descripción debe agregarse");
            txtDescripcion.requestFocus();
            return false;
        }
        if (txtMonto.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "El Monto debe agregarse");
            txtMonto.requestFocus();
            return false;
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtDni = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPatente = new javax.swing.JTextField();
        txtDescripcion = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtMonto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        chkPagoVoluntario = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        btnCargar = new javax.swing.JButton();
        btnVerReportes = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Nombre");

        jLabel2.setText("Ingreso de Multa");

        jLabel3.setText("DNI");

        txtDni.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDniFocusLost(evt);
            }
        });

        jLabel4.setText("Patente");

        jLabel5.setText("Descripción");

        txtMonto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMontoFocusLost(evt);
            }
        });

        jLabel6.setText("Monto");

        jLabel7.setText("Pago Voluntario");

        btnCargar.setText("Cargar");
        btnCargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCargarActionPerformed(evt);
            }
        });

        btnVerReportes.setText("Ver Reportes");
        btnVerReportes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerReportesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(65, 65, 65)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addGap(96, 96, 96)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPatente, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkPagoVoluntario)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnCargar)
                            .addComponent(jLabel2))))
                .addContainerGap(50, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnVerReportes)
                .addGap(66, 66, 66))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPatente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7)
                    .addComponent(chkPagoVoluntario))
                .addGap(43, 43, 43)
                .addComponent(btnCargar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnVerReportes)
                .addContainerGap(62, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCargarActionPerformed
        if (validation()) {
            int dni = Integer.parseInt(txtDni.getText());
            String nombre = txtNombre.getText();
            String patente = txtPatente.getText();
            String descripcionMulta = txtDescripcion.getText();
            float monto = Float.parseFloat(txtMonto.getText());
            boolean pagoVoluntario = chkPagoVoluntario.isSelected();

            Multa m = new Multa (dni, nombre, patente, descripcionMulta, monto, pagoVoluntario);
            caminera.agregarMulta(m);

            txtNombre.setText("");
            txtDni.setText("");
            txtPatente.setText("");
            txtDescripcion.setText("");
            txtMonto.setText("");
            chkPagoVoluntario.setSelected(false);
            btnVerReportes.setEnabled(true);
        }
    }//GEN-LAST:event_btnCargarActionPerformed

    private void btnVerReportesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerReportesActionPerformed
        String cantVoluntarioMasMil = String.valueOf(caminera.cantidadMultasPagoVoluntarioMayor1000());
        String montoGeneral = String.valueOf(caminera.montoGeneral());
        String promedioMontoPagoVoluntario = String.valueOf(caminera.promedioMontoPagoVoluntario());
        
        Reportes repo = new Reportes(cantVoluntarioMasMil, promedioMontoPagoVoluntario, montoGeneral);
        repo.setTitle("Reportes");
        repo.setVisible(true);
    }//GEN-LAST:event_btnVerReportesActionPerformed

    private void txtDniFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDniFocusLost
        String text = txtDni.getText();
        try {
            double x = Integer.parseInt(text);
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "El DNI debe ser un número");
            txtDni.requestFocus();
        }
    }//GEN-LAST:event_txtDniFocusLost

    private void txtMontoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMontoFocusLost
        String text = txtMonto.getText();
        try {
            double x = Float.parseFloat(text);
        } catch (NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "El Monto debe ser un número");
            txtMonto.requestFocus();
        }
    }//GEN-LAST:event_txtMontoFocusLost

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCargar;
    private javax.swing.JButton btnVerReportes;
    private javax.swing.JCheckBox chkPagoVoluntario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtDni;
    private javax.swing.JTextField txtMonto;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPatente;
    // End of variables declaration//GEN-END:variables
}

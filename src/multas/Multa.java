/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multas;

/**
 *
 * @author Maka
 */
public class Multa {
    private int DNI;
    private String nombre;
    private String patente;
    private String descripcion;
    private float monto;
    private boolean pagoVoluntario;

    public Multa() {
    }

    public Multa(int DNI, String nombre, String patente, String descripcion, float monto, boolean pagoVoluntario) {
        if (pagoVoluntario) {
            this.monto = monto/2;
        } else {
            this.monto = monto;
        }
        this.DNI = DNI;
        this.nombre = nombre;
        this.patente = patente;
        this.descripcion = descripcion;
        this.pagoVoluntario = pagoVoluntario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public float getMonto() {
        return monto;
    }
    
    public int getDNI() {
        return DNI;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPatente() {
        return patente;
    }
    
    public boolean isPagoVoluntario() {
        return pagoVoluntario;
    }
    
    public String toStringPagoVoluntario() {
        String voluntario= "No";
        if (isPagoVoluntario()) {
            voluntario = "Si";
        }
        return voluntario;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setMonto(float monto) {
        if (this.isPagoVoluntario()) {
            this.monto = monto/2;
        }
        this.monto = monto;
    }

    public void setPagoVoluntario(boolean pagoVoluntario) {
        this.pagoVoluntario = pagoVoluntario;
    }

    public void setDNI(int DNI) {
        this.DNI = DNI;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    @Override
    public String toString() {
        return "Multa{" + "DNI:" + DNI + ", nombre:" + nombre + ", patente:" + patente + ", descripcion:" + descripcion + ", monto:" + monto + ", pagoVoluntario:" + toStringPagoVoluntario() + '}';
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multas;

import java.util.ArrayList;

/**
 *
 * @author Maka
 */
public class Caminera {
    
    private static ArrayList<Multa> multa; 

    public Caminera() {
        multa = new ArrayList();
    }
    
    public static ArrayList<Multa> getMulta() {
        return multa;
    }
    
    public void agregarMulta(Multa m){
        multa.add(m); 
    }
    
    public int cantidadMultasPagoVoluntarioMayor1000(){
        int cantidad = 0;
        for (Multa m : multa) {
            if (m.isPagoVoluntario() && m.getMonto() > 1000) {
                cantidad ++;
            }
        }
        return cantidad;
    }
    
    public float promedioMontoPagoVoluntario(){
        int contador = 0;
        float acumulador = 0;
        for (Multa m : multa) {
            if (m.isPagoVoluntario()) {
                acumulador += m.getMonto();
                contador ++;
            }
        }
        if(contador==0) return 0;
        else return (acumulador/contador);
    }
    
    public float montoGeneral(){
        float total = 0;
        for (Multa m : multa) {
            total += m.getMonto();
        }
        return total;
    }

}

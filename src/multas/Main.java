/*
 El ministerio de seguridad de la provincia de Córdoba nos solicita una aplicación con ventanas que permita
cargar las multas de la caminera. Se necesita registrar dni del conductor, nombre del conductor, patente
del auto, descripcion de la multa y monto de la multa. Cuando se realiza el pago voluntario por parte del
infractor la multa tiene un descuento del 50%. Realizar una pantalla de carga de multas y que la misma
permita abrir otra ventana con los siguientes reportes:
• Cantidad de multas que tuvieron pago voluntario y que dicho monto pagado sea mayor a $1000.
• Suma de monto pagado general
• Promedio de monto pagado por pago voluntario
 */
package multas;

/**
 *
 * @author Maka
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Principal ventana = new Principal();
        
        ventana.setTitle("Multas");
        ventana.setVisible(true);
        ventana.setSize(500, 500);
    }
    
}
